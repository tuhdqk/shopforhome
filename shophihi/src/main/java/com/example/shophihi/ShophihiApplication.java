package com.example.shophihi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ShophihiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShophihiApplication.class, args);
	}

}
