package com.example.shophihi.Controller;

import java.util.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import com.example.shophihi.Config.*;
import com.example.shophihi.Model.*;
import com.example.shophihi.Service.*;
import org.springframework.http.*;

@RestController
@RequestMapping(path="api/products")
public class ProductController{
    @Autowired
    Tools tool;

    @Autowired
    ProductService product;
    @Autowired
    ProductImageService pim;
    @Autowired
    CategoryService categoryService;

    @GetMapping("category/{id}")
    public List<Product[]> a(@PathVariable("id") int  id)
    {
        Category aa = new Category();
       aa.setId(id);
       return product.getProductByCategory(aa);
       
    }

    @GetMapping("/getAllProduct")
    public List<Product> getAllProduct() {
        
        return product.getAllProduct();
    }
    @GetMapping("/{id}")
    public Product productDetail(@PathVariable("id") Integer  id) {
        
        return product.getProductDetail(id);
    }

    @PostMapping("auth/create")
    public Product CreateProductByFile(@RequestPart("info") Product product ,@RequestParam("file") MultipartFile[] file) {

        String uuid = UUID.randomUUID().toString();
        ArrayList<String> image=new ArrayList<String>();
        for(int i=0;i<file.length;i++)
        {
            String filename = "user\\"+uuid+"."+file[i].getOriginalFilename().substring(file[i].getOriginalFilename().lastIndexOf(".") + 1);
            image.add(filename);
        }
        
        return product;
    }

    @PostMapping("/create")
    public Product productCreate(@RequestParam("idCategory") int idCategory, @RequestPart("info") Product products ,@RequestParam("file") MultipartFile[] file) {
        String uuid = UUID.randomUUID().toString();
        Category cate =categoryService.getCategoryDetail(idCategory);
        products.setCategory(cate);
        Product p = product.create(products);
        int idpro = p.getId();
        if(file==null)
        {
            System.out.println("file null");
        }
        else{
           
            for(int i =0;i<file.length;i++)
            {
                ProductImage pi = new ProductImage();
                pi.setProduct(product.getProductDetail(idpro));
                pi.setImageLink("product\\"+uuid+"\\"+file[i].getOriginalFilename());
                pim.create(pi);
                tool.uploadFile(file[i],"product/"+uuid+"/"+file[i].getOriginalFilename());

            }
        }
        return p;
    }

    @PutMapping("edit/{id}")
    public Product edit(@RequestParam("idCategory") int idCategory, @RequestPart("info") Product products,
    @PathVariable("id") Integer  id
    ,@RequestParam("file") MultipartFile[] file)
    {
        Product pro= product.getProductDetail(id);

        String uuid = UUID.randomUUID().toString();
        if(file==null)
        {
            System.out.println("file null");
        }
        else{
            List<ProductImage> imp = pim.getImageByProduct(pro);
            for(int i =0;i<imp.size();i++){
                pim.delete(imp.get(i));
            }
            for(int i =0;i<file.length;i++)
            {
                ProductImage pi = new ProductImage();
                pi.setProduct(pro);
                pi.setImageLink("product\\"+uuid+"\\"+file[i].getOriginalFilename());
                pim.create(pi);
                tool.uploadFile(file[i],"product/"+uuid+"/"+file[i].getOriginalFilename());

            }
        }
        for(int i =0;i<file.length;i++)
        {
            tool.uploadFile(file[i],"product/"+uuid+"/"+file[i].getOriginalFilename());

        }
        Category cate =categoryService.getCategoryDetail(idCategory);
        pro.setCategory(cate);
        pro.setName(products.getName());
        pro.setQuantity(products.getQuantity());
        pro.setPrice(products.getPrice());
        pro.setDescription(products.getDescription());
        return product.edit(pro);

    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Product> delete(@PathVariable("id") int id )
    {
        return new ResponseEntity(product.delete(id), HttpStatus.OK);
    }

}
