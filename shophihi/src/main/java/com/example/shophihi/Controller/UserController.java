package com.example.shophihi.Controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.shophihi.Config.Tools;
import com.example.shophihi.JWT.JwtTokenProvider;
import com.example.shophihi.Model.User;
import com.example.shophihi.Service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {
    
    HttpStatus status;

    @Autowired
    Tools tool;

    @Autowired
    UserService uService;

    @Autowired
    JwtTokenProvider tokenProvider;

    @GetMapping("/getAllUser")
    public List<User> getAllUser() {
        return uService.getAllUser();
    }

    @GetMapping("/{id}")
    public User userDetail(@PathVariable("id") Integer  id) {
        
        return uService.getUserDetail(id);
    }

    @PostMapping("/login")
    public User authenticateUser(@RequestBody User  users) {
        users.setPassWord(tool.encryption(users.getPassWord()));
        users=uService.login(users);
       if(users!=null)
       {
        // Trả về jwt cho người dùng.
        String jwt = tokenProvider.generateToken( users);
        users.setToken(jwt);
        return users;
       }
       throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Wrong username or password");
     
        
    }
  
    @PostMapping(value="/register")
    public ResponseEntity<User> register(@RequestPart("info") User users ,@RequestParam("file") MultipartFile file ) {
 
        try
        {
            String uuid = UUID.randomUUID().toString();
            String filename = "user\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            tool.uploadFile(file, filename);
            users.setImage(filename);
            users.setPassWord(tool.encryption(users.getPassWord()));
            users=  (User) uService.register(users); 
            return new ResponseEntity(users, HttpStatus.CREATED);

        }
        catch(Exception e)
        {
            System.out.print(e+"loi");
            String error=(String) uService.register(users);
            throw new ResponseStatusException(HttpStatus.CONFLICT,error);   
            
        }
    }
    @PutMapping("edit/{id}")
    public User edit( @RequestPart("info") User users,
    @PathVariable("id") Integer  id
    ,@RequestParam("file") MultipartFile file)
    {
        if(file==null)
        {
            System.out.println("file null");
        }
        users.setId(id);
        String uuid = UUID.randomUUID().toString();
        String filename = "user\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        tool.uploadFile(file, filename);
        users.setImage(filename);
        
        return uService.edit(users);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<User> delete(@PathVariable("id") int id )
    {
        return new ResponseEntity(uService.delete(id), HttpStatus.OK);
    }

}
