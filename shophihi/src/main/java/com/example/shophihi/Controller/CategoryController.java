package com.example.shophihi.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.shophihi.Model.Category;
import com.example.shophihi.Service.CategoryService;

@RestController
@RequestMapping(path="/api/category")
public class CategoryController {
    @Autowired
    CategoryService category;

    @GetMapping("/getAllCategory")
    public List<Category> getAllCategory() 
    {
        return category.getAllCategory();
    }
    @GetMapping("/{id}")
    public Category categoryDetail(@PathVariable("id") Integer  id) {
        
        return category.getCategoryDetail(id);
    }

    @PostMapping("/create")
    public Category productCreate(@RequestPart("info") Category categories ) {
        category.create(categories);
        return categories;
    }

    @PutMapping("edit/{id}")
    public Category edit( @RequestBody Category categories,
    @PathVariable("id") Integer  id)
    {
        categories.setId(id);
        System.out.print(categories.getNameCategory()+"ok");
        return category.edit(categories);
    }

    @DeleteMapping("delete/{id}")
    public void delete(@PathVariable("id") int id )
    {
        category.delete(id);
    }
    
}
