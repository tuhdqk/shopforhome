package com.example.shophihi.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.shophihi.Model.Admin;
import com.example.shophihi.Service.AdminService;

@RestController
@RequestMapping(path="/api/admin")
public class AdminController {
    @Autowired
    AdminService admins;

    @GetMapping("/getAllAdmin")
    public List<Admin> getAllAdmin() {
        return admins.getAllAdmin();
    }

    @GetMapping("/admin/{id}")
    public Admin userDetail(@PathVariable("id") Integer  id) {
        
        return admins.getAdminDetail(id);
    }

}

