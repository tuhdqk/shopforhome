package com.example.shophihi.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.shophihi.Model.Coupon;
import com.example.shophihi.Service.CouponService;

@RestController
@RequestMapping(path="/api/coupon")
public class CouponController {
    @Autowired
    CouponService coupon;

    @GetMapping("/getAllCategory")
    public List<Coupon> getAllCoupon() 
    {
        return coupon.getAllCoupon();
    }
}