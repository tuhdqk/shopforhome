package com.example.shophihi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import org.springframework.stereotype.Repository;

import com.example.shophihi.Model.Product;
import com.example.shophihi.Model.ProductImage;

@Repository
public interface ProductImageRepository extends JpaRepository<ProductImage, Integer > {
    public List<ProductImage> findByProduct(Product p);
}

