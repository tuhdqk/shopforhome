package com.example.shophihi.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.shophihi.Model.Category;
import com.example.shophihi.Model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{
    public List<Product[]> findByCategory(Category c);
}
