package com.example.shophihi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.shophihi.Model.Bill;

@Repository
public interface BillRepository extends JpaRepository<Bill, Integer >{
}

