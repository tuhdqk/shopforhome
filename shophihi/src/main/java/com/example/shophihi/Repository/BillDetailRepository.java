package com.example.shophihi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.shophihi.Model.BillDetail;

@Repository
public interface BillDetailRepository extends JpaRepository<BillDetail, Integer > {
    
}
