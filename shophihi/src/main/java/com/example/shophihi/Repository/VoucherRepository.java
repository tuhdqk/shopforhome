package com.example.shophihi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.shophihi.Model.Voucher;

@Repository
public interface VoucherRepository extends JpaRepository<Voucher, Integer>{
    
}

