package com.example.shophihi.Service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.shophihi.Model.Coupon;
import com.example.shophihi.Repository.CouponRepository;

@Service
public class CouponService {
    @Autowired
    CouponRepository rep;

    public List<Coupon> getAllCoupon()
    {
      return rep.findAll();
    }
}

