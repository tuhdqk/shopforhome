package com.example.shophihi.Service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.shophihi.Model.Admin;
import com.example.shophihi.Repository.AdminRepository;

@Service
public class AdminService {
    @Autowired
    AdminRepository rep;

    public List<Admin> getAllAdmin()
    {
      return rep.findAll();
    }

    public Admin getAdminDetail(int id) {

        return rep.findById(id).get();
    }

    public Admin create(Admin admin) {
        return rep.save(admin);
    }
}

