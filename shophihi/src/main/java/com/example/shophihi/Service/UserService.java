package com.example.shophihi.Service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.shophihi.Model.CustomUserDetails;
import com.example.shophihi.Model.User;
import com.example.shophihi.Repository.UserRepository;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository rep;

    String unExists="Username allready exists";

    public List<User> getAllUser()
    {
      return rep.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        // TODO Auto-generated method stub
        User user = rep.findByUserName(userName);
        if (user == null) {
            throw new UsernameNotFoundException(userName);
        }
        return new CustomUserDetails(user);
    }

    public UserDetails loadUserById(String Id) {
		User user = rep.findById(Integer.parseInt(Id)).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + Id)
            );

            return new CustomUserDetails(user);

	}


    public User getUserDetail(int id) {
        
 
        return rep.findById(id).get();
    }
    public boolean checkUsername(String userName)
    {
      return rep.findByUserName(userName) == null ;
    }

    public User login(User user) {

        return rep.findByUserName(user.getUserName());
    }

    public User create(User user) {
        return rep.save(user);
    }

    public Object register(User u)
    {
        User un=rep.findByUserName(u.getUserName());
        
        if(un==null)
        {
          u=  rep.save(u);
        }
        else return unExists;

        return u;
    }
    public User edit(User u) {
        Optional<User> existing=rep.findById(u.getId());

		if(existing.isPresent())
		{
            System.out.println("edit");
			User old=existing.get();
			old.setName(u.getName());
			old.setEmail(u.getEmail());
			old.setPhone(u.getPhone());

            old.setPassWord(u.getPassWord());
            old.setImage(u.getImage());
            rep.save(old);
			return old;

		}
		else
			return null;
    }
    public String delete(int id) {
        User a=rep.findById(id).orElseThrow(()-> 
        new UsernameNotFoundException("User not found with id : " + id)
        );
		rep.delete(a);
        return "done";
    }
}

