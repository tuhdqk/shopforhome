package com.example.shophihi.Service;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.shophihi.Model.Category;
import com.example.shophihi.Repository.CategoryRepository;

@Service
public class CategoryService {
    @Autowired
    CategoryRepository rep;

    public List<Category> getAllCategory()
    {
      return rep.findAll();
    }

    public Category getCategoryDetail(int id) {
        //TODO-get product detail

        return rep.findById(id).get();
    }

    public void create(Category c)
    {
        c=rep.save(c);
    }
    public Category edit(Category c) {
      Optional <Category> existing=rep.findById(c.getId());
      
      if(existing != null){
      Category o = existing.get();
      o.setNameCategory(c.getNameCategory());			
      
			return rep.save(o);
      
    }else return null;
    }

    public void delete(int id) {
    Category a=rep.findById(id).get();
		rep.delete(a);
    }

}

