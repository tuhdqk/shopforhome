package com.example.shophihi.Service;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.shophihi.Model.Product;
import com.example.shophihi.Model.ProductImage;
import com.example.shophihi.Repository.ProductImageRepository;

@Service
public class ProductImageService {
    @Autowired
    ProductImageRepository rep;

    public List<ProductImage> getImageByProduct(Product ca)
    {
      return rep.findByProduct(ca);
    }
    public ProductImage create(ProductImage u)
    {
        return u=rep.save(u);
    }
    public String delete(ProductImage u)
    {
        rep.delete(u);
        return "done";
    }
    public ProductImage getImageById(int id)
    {
      return rep.findById(id).get();
    }

}

