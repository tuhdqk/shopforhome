package com.example.shophihi.Service;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.shophihi.Model.Category;
import com.example.shophihi.Model.Product;
import com.example.shophihi.Repository.ProductRepository;

@Service
public class ProductService {
    @Autowired
    ProductRepository rep;

    public List<Product> getAllProduct()
    {
      return rep.findAll();
    }

    public Product getProductDetail(int id) {
        //TODO-get product detail

        return rep.findById(id).get();
    }

    public List<Product[]> getProductByCategory(Category ca) {
        //TODO-get product by category

        return rep.findByCategory(ca);
    }

    public Product getDetail(int id)
    {
        return rep.findById(id).get();
    }

    public Product create(Product u)
    {
        return u=rep.save(u);
    }
    public Product edit(Product u) {
        return u=rep.save(u);
    }

    public String delete(int id) {
        Optional<Product> existing=rep.findById(id);
        if(existing.isPresent()){
            rep.delete(existing.get());
            return "done";
        }
        else return "no found product id";
    }
}
