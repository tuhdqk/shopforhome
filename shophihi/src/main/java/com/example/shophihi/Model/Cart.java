package com.example.shophihi.Model;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;


@Entity
@Getter
@Setter
@Data
@Table(name = "cart")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @ManyToOne
    @JsonIgnore
    User user;

    @ManyToOne
    Product product;

}
