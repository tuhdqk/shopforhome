package com.example.shophihi.Model;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "productimage")
public class ProductImage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @ManyToOne
    @JsonIgnore
    Product product;

    @Column(length=500)
    String imageLink;

}
