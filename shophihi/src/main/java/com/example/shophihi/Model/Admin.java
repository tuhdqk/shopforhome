package com.example.shophihi.Model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "admin")
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @NotNull(message = "not null")
    @Column(length=100)
    String name;

    @NotNull(message = "not null")
    @Column(length=100)
    String email;
    
    @NotNull(message = "not null")
    @Column(length=100)
    String phone;
    
    @NotNull(message = "not null")
    @Column(length=100)
    String userName;
    
    @NotNull(message = "not null")
    @Column(length=100)
    String passWord;
    
    @Column(length=500)
    String image;

    @OneToMany(mappedBy = "admin")
    List<Bill> bill;

}
