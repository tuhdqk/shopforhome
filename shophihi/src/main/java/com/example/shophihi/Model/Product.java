package com.example.shophihi.Model;

import javax.persistence.*;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "product")
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  int id;

  @ManyToOne
  @JsonIgnore
  Category category;

  @NotNull(message = "not be null")
    @Column(length=100)
    String name; 

    @NotNull(message = "not be null")
    int quantity; 

    @NotNull(message = "not be null")
    int price;

    @Column(length=500)
    String description;

  @OneToMany(mappedBy = "bill")
  @JsonIgnore
  List<BillDetail> billDetail;

  @OneToMany(mappedBy = "user")
  @JsonIgnore
  List<Cart> cart;  

  @OneToMany(mappedBy = "product")
  @JsonIgnore
  List<ProductImage> productimage;


}
