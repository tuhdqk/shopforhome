package com.example.shophihi.Model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "bill")
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @ManyToOne
    @JsonIgnore
    Admin admin;

    @ManyToOne
    @JsonIgnore
    User user;

    @NotNull(message = "not null")
    @Column(length=100)
    String date;

    @NotNull(message = "not null")
    @Column(length=500)
    String address;

    @NotNull(message = "not null")
    int idCoupon;

    @NotNull(message = "not null")
    int total;
    
    @OneToMany(mappedBy = "bill")
    List<BillDetail> billDetail;

}
