package com.example.shophihi.Model;

import javax.persistence.*;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "billdetail")
public class BillDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @ManyToOne
    @JsonIgnore
    Bill bill; 

    @ManyToOne
    Product product;

    @NotNull(message = "not null")
    int quantity;

    @NotNull(message = "not null")
    int total;

}
