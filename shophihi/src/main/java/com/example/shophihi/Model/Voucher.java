package com.example.shophihi.Model;

import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "voucher")
public class Voucher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @NotNull(message = "not null")
    int idUser;

    @NotNull(message = "not null")
    int idCoupon;

}
