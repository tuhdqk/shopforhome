package com.example.shophihi.Model;

import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "coupon")
public class Coupon {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @NotNull(message = "not null")
    @Column(length=100)
    String nameCoupon;

    @NotNull(message = "not null")
    int discount;

}
