package com.example.shophihi.Model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @NotNull(message = "not null")
    @Column(length=100)
    String nameCategory;

    @OneToMany(mappedBy = "category")
    List<Product> product;


}
