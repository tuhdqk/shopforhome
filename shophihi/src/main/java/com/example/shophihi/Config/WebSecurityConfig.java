package com.example.shophihi.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class WebSecurityConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
						http.csrf().disable()
						// dont authenticate this particular request
						.authorizeRequests().
						antMatchers("/api/**").permitAll().
						antMatchers("/api/**/auth/**").authenticated().
						// all other requests need to be authenticated
						and();
        http.headers().frameOptions();
		return http.build();

    }

	
}